import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageDetailComponent } from './image-details.component';
import { ImageService } from '../image.service';
import { RouterTestingModule } from '@angular/router/testing';


class mockImageService {
  getImages() { return [{ "id": 1, brand: 'prueba' }]; }
  getImage(id: number) { return []; }
}

describe('ImageDetailsComponent', () => {
  let component: ImageDetailComponent;
  let fixture: ComponentFixture<ImageDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ImageDetailComponent],
      providers: [
        {
          provide: ImageService,
          useClass: mockImageService
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Debería crear el componente correctamente', () => {
    expect(component).toBeTruthy();
    console.log(component.image);
  });
});
