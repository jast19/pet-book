import { FilterimagesPipe } from './filterimages.pipe';
const images = [
  { "id": 1, "brand": "perro", "url": "assets/images/perro1.jpg" },
  { "id": 2, "brand": "perro", "url": "assets/images/perro2.jpg" },
  { "id": 3, "brand": "gato", "url": "assets/images/gato1.jpg" },
  { "id": 4, "brand": "gato", "url": "assets/images/gato2.jpeg" },
  { "id": 5, "brand": "perro", "url": "assets/images/perro3.jpg" },
]
describe('FilterimagesPipe', () => {

  it('Debería retornar un arreglo de perros cuando el parámetro de filtro es "perro"', () => {
    const pipe = new FilterimagesPipe();
    try {
      testFilter(pipe, 'perro', 3);
    } catch (e) { throw e }
  })
  it('Debería retornar un arreglo de perros cuando el parámetro de filtro es "gato"', () => {
    const pipe = new FilterimagesPipe();
    try {
      testFilter(pipe, 'gato', 2);
    } catch (e) { throw e }
  })
  it('Debería retornar un arreglo de todas las imágenes cuando el parámetro de filtro es "all"', () => {
    const pipe = new FilterimagesPipe();
    const result: Array<object> = pipe.transform(images, 'all');
    expect(result).toEqual(images);
  })
  it('Debería retornar un arreglo vacío cuando el parámetro de filtro sea "aoisjbdnajsbd"', () => {
    const pipe = new FilterimagesPipe();
    const result: Array<object> = pipe.transform(images, 'aoisjbdnajsbd');
    expect(result).toBeInstanceOf(Array);
    expect(result.length).toEqual(0);
  })
});

function testFilter(pipeInstance, filter, length) {
  const result: Array<object> = pipeInstance.transform(images, filter);
  expect(result).toBeInstanceOf(Array);
  expect(result.length).toEqual(length);
  result.every((item) => { expect(item['brand']).toEqual(filter) });
}